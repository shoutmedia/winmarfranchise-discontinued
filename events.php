<?php
/*
 * Template Name: Events
 */
get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php include(get_template_directory() . '/inc/banner.php'); ?>
		<main class="main_content">
			<div class="container">
				<div class="events-manager">
					<?php the_content(); ?>
				</div>
			</div>
		</main>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>
