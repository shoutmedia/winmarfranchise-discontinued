<?php

$background    = $block['background_image'];
$bgcolor       = $block['background_color'];
$bgposition    = $block['background_position'];
$style         = $block['text_style'];
$alignment     = $block['text_alignment'];
$colsize       = $block['text_column_size'];
$title         = $block['title'];
$titlestyle    = $block['title_style'];
$desc          = $block['description'];
$btns          = $block['buttons'];
$accent        = $block['accent_image'];
$accent_video  = $block['accent_video'];
$accent_type   = $block['accent_type'];
$bg_type 	   = $block['background_type'];
$bg_video_webm = $block['background_video_webm'];
$bg_video_mp4  = $block['background_video_mp4'];
$darken_bg     = $block['darken_background'];
$fit_image     = $block['fit_image'];
$title_width   = $block['title_width'];


if($accent) :
	$accsrc = wp_get_attachment_image_src($accent, array(760, 760));
	$accmet = wp_get_attachment($accent);
endif;

if($background) :
	if($bgposition == 'fill')
		$bgsrc = wp_get_attachment_image_src($background, array(2000, 2000));
	else
		$bgsrc = wp_get_attachment_image_src($background, 'full');
endif;

?>
<div class="featured_image<?php echo $bgposition ? ' ' . $bgposition : ''; ?><?php echo $darken_bg ? ' darken' : ''; ?>"<?php

	if($background || $bgcolor) :
		echo ' style="';
		echo $background ? 'background-image: url(' . $bgsrc[0] . ');' : '';
		echo $bgcolor    ? 'background-color: ' . $bgcolor . ';' : '';
		echo '"';
	endif;

	?>>
	<?php if($bg_type == 'Video'): ?>
	<video playsinline autoplay muted loop poster="" class="bgvid">
	    <source src="<?php echo $bg_video_webm; ?>" type="video/webm">
	    <source src="<?php echo $bg_video_mp4; ?>" type="video/mp4">
	</video>
	<?php endif; ?>
	<div class="container">
		<?php if($title && $title_width === 'title-full') : ?>
			<div class="title_block<?php echo $titlestyle ? ' ' . $titlestyle : ''; ?> title-full  <?php echo $style; ?>">
				<?php if($titlestyle == 'style2') :

					$title_split = split_line($title);

					?>
					<h2 class="title"><strong><?php echo $title_split[0]; ?></strong> <?php echo $title_split[1]; ?></h2>
				<?php else : ?>
					<h2 class="title"><?php echo $title; ?></h2>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<div class="inner<?php echo $alignment == 'center' ? ' centered' : ''; ?>">
			<?php if($alignment == 'right') : ?>
				<?php if($accent_type == 'image'): ?>
					<div class="accent"
					<?php
						if($accent) {
							echo ' style="background-image: url(' . $accsrc[0] . ');';
							if($fit_image) {
								echo ' background-size: contain;';
							}
							echo '"';
						}
					?>
					>
						<?php if($accent) : ?>
							<img src="<?php echo $accsrc[0]; ?>" alt="<?php echo $accmet['alt']; ?>">
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if($accent_type == 'video'): ?>
					<div class="accent">
						<?php echo $accent_video; ?>
					</div>
				<?php endif; ?>
			<?php endif; ?>

			<div class="text <?php echo $style; ?> s<?php echo $colsize; ?>">
				<?php if($title && $title_width !== 'title-full') : ?>
					<div class="title_block<?php echo $titlestyle ? ' ' . $titlestyle : ''; ?>">
						<?php if($titlestyle == 'style2') :

							$title_split = split_line($title);

							?>
							<h2 class="title"><strong><?php echo $title_split[0]; ?></strong> <?php echo $title_split[1]; ?></h2>
						<?php else : ?>
							<h2 class="title"><?php echo $title; ?></h2>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php echo $desc; ?>
				<?php if($btns) : ?>
					<div class="buttons">
						<?php foreach($btns as $btn) : ?>
							<a href="<?php echo $btn['destination']; ?>" class="btn <?php echo $btn['style']; ?>"><?php echo $btn['label']; ?></a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>

			<?php if($alignment == 'left') : ?>
				<?php if($accent_type == 'image'): ?>
					<div class="accent"
					<?php
						if($accent) {
							echo ' style="background-image: url(' . $accsrc[0] . ');';
							if($fit_image) {
								echo ' background-size: contain;';
							}
							echo '"';
						}
					?>
					>
						<?php if($accent) : ?>
							<img src="<?php echo $accsrc[0]; ?>" alt="<?php echo $accmet['alt']; ?>">
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if($accent_type == 'video'): ?>
					<div class="accent">
						<?php echo $accent_video; ?>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
