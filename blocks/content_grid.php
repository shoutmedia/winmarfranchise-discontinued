<?php

$overlap    = $block['overlap'];
$services   = $block['items'];
$titlestyle = $block['title_style'];
$title      = $block['title'];

$background   = $block['background_image'];
$bgcolor      = $block['background_color'];
$bgposition   = $block['background_position'];

if($background) :
    if($bgposition == 'fill')
        $bgsrc = wp_get_attachment_image_src($background, array(2000, 2000));
    else
        $bgsrc = wp_get_attachment_image_src($background, 'full');
endif;

?>
<div class="content_grid<?php echo $overlap ? ' overlap' : ''; ?>"
<?php

    if($background || $bgcolor) :
        echo ' style="';
        echo $background ? 'background-image: url(' . $bgsrc[0] . ');' : '';
        echo $bgcolor    ? 'background-color: ' . $bgcolor . ';' : '';
        echo '"';
    endif;

?>>
    <div class="container">
        <?php if($title): ?>
        <div class="title_block<?php echo $titlestyle ? ' ' . $titlestyle : ''; ?>">
            <h2 class="title"><?php echo $title; ?></h2>
        </div>
        <?php endif; ?>
        <div class="services">
            <div class="row">
                <?php foreach($services as $key => $post) :

                    if($key > 0 && $key % 3 == 0)
                        echo '</div><div class="row">';

                    if($key > 0 && $key % 3 != 0)
                        echo '<div class="spacer"></div>';

                    if($post['image'])
                        $src = wp_get_attachment_image_src($post['image'], [425, 425]);

                    $show_text = false;
                    if($post['label'] || $post['description'] || $post['buttons']) {
                        $show_text = true;
                    }

                    ?>
                    <div class="service">
                        <div class="inner"></div>
                        <?php if($post['image']) : ?>
                            <div class="img<?php if(!$show_text) echo ' no-text'; ?>" style="background-image: url(<?php echo $src[0]; ?>);"></div>
                        <?php endif; ?>
                        <?php if($show_text): ?>
                        <div class="text">
                            <?php if($post['label']): ?>
                            <h3 class="label"><?php echo $post['label']; ?></h3>
                            <?php endif; ?>
                            <?php echo $post['description']; ?>
                            <?php if($post['buttons']) : ?>
                                <div class="btns">
                                    <?php foreach($post['buttons'] as $btn) : ?>
                                        <a href="<?php echo $btn['destination']; ?>" class="btn"><?php echo $btn['label']; ?></a>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                        <?php if(has_post_thumbnail()) :

                            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), array(450, 450));

                            ?>
                            <div class="accent">
                                <div class="img" style="background-image: url(<?php echo $thumb[0]; ?>);"></div>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</div>