var gulp       = require('gulp');
var sass       = require('gulp-sass');
var prefix     = require('gulp-autoprefixer');
var minifycss  = require('gulp-clean-css');
var ttf2eot    = require('gulp-ttf2eot');
var ttf2woff   = require('gulp-ttf2woff');
var ttf2woff2  = require('gulp-ttf2woff2');
var plumber    = require('gulp-plumber');
var imagemin   = require('gulp-imagemin');
var sourcemaps = require('gulp-sourcemaps');
var combineMQ  = require('gulp-group-css-media-queries');
var watch      = require('gulp-watch');
var concat     = require('gulp-concat');
var uglify     = require('gulp-uglify');
var otf2ttf    = require('otf2ttf');
var sync       = require('browser-sync').create();
var runSeq     = require('run-sequence');
var del        = require('del');
var fs         = require('fs');

var config = {
    url   : 'winmarfranchise.vm',
    ports : {
        web : 3000
    },
    src   : {
        img   : 'src/img/**/*.{png,jpg,gif}',
        js    : 'src/js/**/*.js',
        css   : 'src/scss/**/*.scss',
        fonts : 'src/fonts/',
        php   : [
            '*.php',
            'blocks/**/*.php',
            'banners/**/*.php',
            'inc/**/*.php'
        ]
    },
    dest  : {
        img   : 'img',
        js    : 'js/',
        css   : '.',
        fonts : 'fonts/'
    }
}

// --- [DEV TASKS] ---

gulp.task('sync', function() {
    sync.init({
        proxy     : config.url,
        port      : config.ports.web,
        ui        : false,
        online    : true,
        logPrefix : "Arcadia",
        open      : false
    });
});

gulp.task('watch', function() {
    // CSS
    watch(config.src.css, function() {
        runSeq('styles');
    });

    // PHP
    watch(config.src.php, function() {
        sync.reload();
    });
});

gulp.task('styles', function() {
    return gulp.src(config.src.css)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle     : 'compact'
        }).on('error', sass.logError))
        .pipe(prefix({
            remove   : false,
            cascade  : false
        }))
        .pipe(plumber.stop())
        .pipe(sourcemaps.write(config.dest.css))
        .pipe(gulp.dest(config.dest.css))
        .pipe(sync.stream({
            match: '**/*.css'
        }));
});

gulp.task('ttf2dist', function() {
    return gulp.src(config.src.fonts + '*.ttf')
        .pipe(gulp.dest(config.dest.fonts));
});

gulp.task('otf2ttf', function() {
    return gulp.src(config.src.fonts + '*.otf')
        .pipe(otf2ttf())
        .pipe(gulp.dest(config.src.fonts));
});

gulp.task('ttf2eot', function() {
    return gulp.src(config.src.fonts + '*.ttf')
        .pipe(ttf2eot())
        .pipe(gulp.dest(config.dest.fonts));
});

gulp.task('ttf2woff', function() {
    return gulp.src(config.src.fonts + '*.ttf')
        .pipe(ttf2woff())
        .pipe(gulp.dest(config.dest.fonts));
});

gulp.task('ttf2woff2', function() {
    return gulp.src(config.src.fonts + '*.ttf')
        .pipe(ttf2woff2())
        .pipe(gulp.dest(config.dest.fonts));
});

gulp.task('images', function() {
    return gulp.src(config.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(config.dest.img));
});

gulp.task('javascript', function() {
    return gulp.src(config.src.js)
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.dest.js))
        .pipe(sync.stream());
});

// --- [BUILD TASKS] ---

gulp.task('build-css', function() {
    return gulp.src(config.src.css)
        .pipe(sass({
            outputStyle     : 'compact',
            errLogToConsole : true
        }))
        .pipe(prefix({
            remove   : false,
            cascade  : false
        }))
        .pipe(combineMQ())
        .pipe(minifycss())
        .pipe(gulp.dest(config.dest.css));
});

gulp.task('build-js', function() {
    return gulp.src(config.src.js)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.dest.js));
});

gulp.task('build', function () {
    runSeq(['build-css', 'build-js']);
});

//gulp.task('default', ['sync', 'styles', 'javascript', 'images', 'watch']);

gulp.task('default', ['sync', 'styles', 'watch']);
