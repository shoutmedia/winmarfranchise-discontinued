jQuery(function($) {
	if($("#side-sortables").length > 0)
	{
		var adminbar = $("#wpadminbar").outerHeight();
		var height   = $("#submitdiv").outerHeight();

		// Inject holder
		$("#side-sortables").prepend('<div class="spacer" style="top: ' + adminbar + 'px; height: ' + height + 'px;"></div>');

		$(window).scroll(function() {
			var sidebar  = $("#side-sortables").offset().top - $("#wpadminbar").outerHeight();

			if($("body").scrollTop() > sidebar)
			{
				// Set submit div as fixed
				$("#side-sortables").addClass('fixed');
				$(".spacer").css({
					'height': $("#submitdiv").outerHeight() + 'px',
					'top': $("#wpadminbar").outerHeight() + 'px'
				});
			}
			else
			{
				// Clear fixed position
				$("#side-sortables").removeClass('fixed');
			}
		});
	}
});
