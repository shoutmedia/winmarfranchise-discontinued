/**
 *  This function will render each map when the document is ready (page has loaded)
 *
 *  @param	n/a
 *  @return	n/a
 */
var map = null;

jQuery(function($) {

	/**
	 *  This function will render a Google Map onto the selected jQuery element
	 *
	 *  @param	$el (jQuery element)
	 *  @return	n/a
	 */
	function new_map( $el )
	{
		// var
		var $markers = $el.find('.marker');
		var $center  = $el.find('.map_center');

		// vars
		var args = {
			zoom		     : 16,
			center		     : new google.maps.LatLng(0, 0),
			mapTypeId	     : google.maps.MapTypeId.ROADMAP,
			scrollwheel      : false,
			styles           : [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#193341"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#2c5a71"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#29768a"},{"lightness":-37}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#3e606f"},{"weight":2},{"gamma":0.84}]},{"elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"weight":0.6},{"color":"#1a3541"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#2c5a71"}]}]
		};

		// create map
		map = new google.maps.Map( $el[0], args );

		// add a markers reference
		map.markers = [];

		// add markers
		$markers.each(function() {
			add_marker( $(this), map );
		});

		if($center.length > 0 && $(window).width() > 767)
			set_map_center( $center, map );
		else
			center_map( map );

		// return
		return map;
	}

	/**
	 *  This function will add a marker to the selected Google Map
	 *
	 *  @param	$marker (jQuery element)
	 *  @param	map (Google Map object)
	 *  @return	n/a
	 */
	function add_marker( $marker, map )
	{
		// var
		var label = $marker.data('label');
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

		// create marker
		if(label.length > 0) {
			var marker = new MarkerWithLabel({
				position     : latlng,
				map          : map,
				labelContent : label,
				labelClass   : "map_labels",
				labelAnchor  : new google.maps.Point(-18, 32)
			});
		} else {
			var marker = new google.maps.Marker({
				position : latlng,
				map      : map
			});
		}

		// add to array
		map.markers.push( marker );

		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content	: $marker.html()
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open( map, marker );
			});
		}
	}

	function set_map_center($marker, map)
	{
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
		map.setCenter( latlng );
		map.setZoom( 13 );
	}

	/**
	 *  This function will center the map, showing all markers attached to this map
	 *
	 *  @param	map (Google Map object)
	 *  @return	n/a
	 */
	function center_map( map )
	{
		// vars
		var bounds = new google.maps.LatLngBounds();

		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){

			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

			bounds.extend( latlng );

		});

		// only 1 marker?
		if( map.markers.length == 1 )
		{
			// set center of map
			map.setCenter( bounds.getCenter() );
			map.setZoom( 13 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );

			if($(window).width() > 767) {
				setTimeout(function() {
					map_recenter( -150 );
				}, 300);
			}
		}

		// Remove marker
		// map.markers[0].setMap(null);
	}

	function map_recenter(offsetx, offsety) {
		var zoom = map.getZoom();

		map.setZoom(zoom - 1);

	    var point1 = map.getProjection().fromLatLngToPoint(
	        map.getCenter()
	    );

	    var point2 = new google.maps.Point(
	        ( (typeof(offsetx) == 'number' ? offsetx : 0) / Math.pow(2, map.getZoom()) ) || 0,
	        ( (typeof(offsety) == 'number' ? offsety : 0) / Math.pow(2, map.getZoom()) ) || 0
	    );

	    map.setCenter(map.getProjection().fromPointToLatLng(new google.maps.Point(
	        point1.x - point2.x,
	        point1.y + point2.y
	    )));
	}

	$(document).ready(function() {

		$('.map').each(function() {
			if($(this).is(":visible"))
				map = new_map( $(this) );
		});

		if($(".slides").length > 0)
		{
			// Sort out testimonial heights
			$(".slides").on('init', function(event, slick, direction){
				var height = 0;

				$(this).find('.testimonial').each(function() {
					if ($(this).outerHeight() > height) {
						height = $(this).outerHeight();
					}
				});

				var $this = $(this);

				setTimeout(function() {
					$this.find('.testimonial').css('height', height);
				}, 50);
			});

			$(".slides").slick({
				infinite: true,
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows: true,
				autoplay: true,
				autoplaySpeed: 8000,
				slide: '.testimonial',
				responsive: [
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 1,
							arrows: false
						}
					}
				]
			});
		}

		if($(".inner_banner").length > 0 && $(window).width() > 1023)
		{
			$(window).scroll(function () {

				/** Get the scroll position of the page */
				var scrollPos = $(this).scrollTop();

				/** Scroll and fade out the banner text */
				$(".inner_banner .title_block").css({
					'margin-top' : ( scrollPos / 3 ) + "px",
					'opacity' : 1 - ( scrollPos / 300 ),
					'-ms-filter' : 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + 1 - ( scrollPos / 300 ) + ')'
				});

				/** Scroll the background of the banner */
				$(".inner_banner").css({
					'background-position' : 'center ' + (- scrollPos / 8 ) + "px"
				});
			});
		}

		$(".mobile-nav").click(function() {
			if($(".main_wrapper").hasClass('open'))
			{
				setTimeout(function() {
					$("body").css('overflow', 'auto');
				}, 300);
			}
			else
			{
				$("body").css('overflow', 'hidden');
			}

			$(".main_wrapper").toggleClass('open');
			$(".sidenav").toggleClass('open');
		});

		if ($(".service_breakdown").length > 0) {
            $(".service_breakdown").each(function() {
                if ($(this).find('.service').length > 4) {
                    $(this).find('.services').slick({
                        infinite: false,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        slide: '.service',
                        responsive: [
                            {
                                breakpoint: 767,
                                settings: "unslick"
                            }
                        ]
                    });
                }
            });
        }

        if (localStorage.getItem($(".corner-pop").attr('data-key')) === null) {
            setTimeout(function() {
                $(".corner-pop").addClass('open');
            }, 500);
        }

	});

    $(".corner-pop .fa").click(function() {
        localStorage.setItem($(".corner-pop").attr('data-key'), 'close');
        $(".corner-pop").removeClass('open');
    });

	$('.contact-staff').on('click', function(e){
		e.preventDefault();
		var sm = $(this).closest('.staff-member');

		var name = sm.find('.staff-name').html();
		var email = sm.find('.staff-email').html();

		$('html, body').animate({
	        scrollTop: $("#form_contact2").offset().top
	    }, 500);

		$('.frm_html_container').html('To: ' + name + ' <i class="fa fa-times-circle clear-to" aria-hidden="true"></i>');
		$('#field_email-to').val(email);
	});

	$('.frm_html_container').on('click', '.clear-to', function(e){
		$('.frm_html_container').html('');
		$('#field_email-to').val($('#field_email-to').data('frmval'));
	});

});
