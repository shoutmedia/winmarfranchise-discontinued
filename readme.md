# WINMAR Franchise

Current Version: `1.4`

## Getting started

You will need **Sass** installed. If you are building from command line you will need to run:

````
sass --watch css/main.scss:style.css --style compressed
````

## Features

* [FontAwesome](http://fortawesome.github.io/Font-Awesome/)
* [SASS](http://sass-lang.com)
* [jQuery](http://jquery.com)
* [Modernizr](http://modernizr.com) This build only contains HTML5Shiv please build only the functionality you require