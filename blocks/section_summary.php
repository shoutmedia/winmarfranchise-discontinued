<?php

$background = $block['background'];
$title      = $block['title'];
$desc       = $block['description'];
$btns       = $block['buttons'];
$sections   = $block['sections'];

if($title)
	$lines = split_line($title);

if($background)
	$bgsrc = wp_get_attachment_image_src($background, array(1920,1920));

?>
<div class="section_summary" style="background-image: url(<?php echo $background ? $bgsrc[0] : get_template_directory_uri() . '/img/bg.jpg'; ?>);">
	<div class="container">
		<div class="title_block">
			<?php if($title) : ?>
				<h2 class="title"><strong><?php echo $lines[0]; ?></strong> <span><?php echo $lines[1]; ?></span></h2>
			<?php endif; ?>
			<div class="text">
				<?php echo $desc; ?>
				<?php if($btns) : ?>
					<?php foreach($btns as $btn) :

						$btnline = split_line($btn['label']);

						?>
						<a href="<?php echo $btn['destination']; ?>" class="btn"><strong><?php echo $btnline[0]; ?></strong> <?php echo $btnline[1]; ?></a>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if($sections) : ?>
		<div class="sections">
			<div class="container">
				<div class="secs">
					<?php foreach($sections as $sec) :

						$photo = $sec['photo'];
						$label = $sec['label'];
						$desc  = $sec['description'];
						$link  = $sec['link_destination'];

						if($photo)
							$phsrc = wp_get_attachment_image_src($photo, array(290, 290));

						?>
						<div class="section">
							<?php if($link) : ?>
							<a href="<?php echo $link; ?>">
							<?php endif; ?>
								<?php if($photo) : ?>
									<div class="img" style="background-image: url(<?php echo $phsrc[0]; ?>);"></div>
								<?php endif; ?>
								<div class="inner">
									<?php if($label) : ?>
										<h5><?php echo $label; ?></h5>
									<?php endif; ?>
									<?php echo $desc; ?>
								</div>
							<?php if($link) : ?>
							</a>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>