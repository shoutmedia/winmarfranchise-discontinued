<?php

$title    = $block['title'];
$services = $block['services'];

?>
<div class="service_grid">
	<div class="container">
		<?php if($title) : ?>
			<div class="title_block">
				<h2 class="title"><?php echo $title; ?></h2>
			</div>
		<?php endif; ?>
		<div class="services">
			<div class="row">
				<?php foreach($services as $key => $post) :

					if($key > 0 && $key % 3 == 0)
						echo '</div><div class="row">';
					if($key > 0 && $key % 3 != 0)
						echo '<div class="spacer"></div>';

					setup_postdata($post);

					$summary = get_field('service_summary');

					?>
					<div class="service">
						<div class="inner"></div>
						<div class="text">
							<h3 class="label"><?php the_title(); ?></h3>
							<?php echo $summary; ?>
							<div class="btns">
								<a href="<?php the_permalink(); ?>" class="btn"><?php _e('View Service', DOMAIN); ?></a>
							</div>
						</div>
						<?php if(has_post_thumbnail()) :

							$thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), array(450, 450));

							?>
							<div class="accent">
								<div class="img" style="background-image: url(<?php echo $thumb[0]; ?>);"></div>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</div>