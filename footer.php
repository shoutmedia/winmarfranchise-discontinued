        <footer class="footer">
            <div class="container">
                <div class="columns x5">
                    <div class="column x2 menu">
                        <h6 class="widget-title"><?php _e('Menu', DOMAIN); ?></h6>
                        <?php wp_nav_menu(array('theme_location' => 'footer1')); ?>
                        <?php wp_nav_menu(array('theme_location' => 'footer2')); ?>
                    </div>
                    <div class="column x1 contact">
                        <h6 class="widget-title"><?php _e('Contact Us', DOMAIN); ?></h6>
                        <ul>
                            <?php if (of_get_option('contact_phone')) : ?>
                                <li>
                                    <div class="icon"><em class="fa fa-phone"></em></div>
                                    <div class="text">
                                        <a href="tel:<?php echo preg_replace('/[^0-9]/', '', of_get_option('contact_phone')); ?>">
                                            <?php echo of_get_option('contact_phone'); ?>
                                        </a>
                                    </div>
                                </li>
                            <?php endif; ?>
                            <?php if (of_get_option('contact_toll_free')) : ?>
                                <li>
                                    <div class="icon"><em class="fa fa-phone"></em></div>
                                    <div class="text"><?php echo of_get_option('contact_toll_free'); ?></div>
                                </li>
                            <?php endif; ?>
                            <?php if (of_get_option('contact_fax')) : ?>
                                <li>
                                    <div class="icon"><em class="fa fa-fax"></em></div>
                                    <div class="text"><?php echo of_get_option('contact_fax'); ?></div>
                                </li>
                            <?php endif; ?>
                            <?php if (of_get_option('contact_email_footer')) : ?>
                                <li>
                                    <div class="icon"><em class="fa fa-envelope-o"></em></div>
                                    <div class="text"><a href="mailto:<?php echo of_get_option('contact_email'); ?>"><?php echo of_get_option('contact_email'); ?></a></div>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <hr>
                        <div class="address">
                        <?php if (of_get_option('contact_address_str') && of_get_option('contact_address_city') && of_get_option('contact_address_province') && of_get_option('contact_address_postal')) : ?>
                            <p><?php echo of_get_option('contact_address_str'); ?><br>
                            <?php if (of_get_option('contact_address_str2')) : ?>
                                <?php echo of_get_option('contact_address_str2'); ?><br>
                            <?php endif; ?>
                            <?php echo of_get_option('contact_address_city'); ?>, <?php echo of_get_option('contact_address_province'); ?><br>
                            <?php echo of_get_option('contact_address_postal'); ?>, Canada</p>
                        <?php endif; ?>
                        <?php if (of_get_option('contact_us_link')) :
                            $contact_page = get_post(of_get_option('contact_us_link'));
                            $url = get_permalink(of_get_option('contact_us_link')); ?>
                            <strong><a href="<?php echo $url; ?>"><?php echo $contact_page->post_title; ?></a></strong>
                        <?php endif; ?>
                        </div>
                    </div>
                    <div class="column x1 social">
                        <h6 class="widget-title"><?php _e('Social Media', DOMAIN); ?></h6>
                        <ul>
                            <?php if (of_get_option('social_facebook')) : ?>
                                <li>
                                    <div class="icon"><a href="<?php echo of_get_option('social_facebook'); ?>" target="_blank"><em class="fa fa-facebook"></em></a></div>
                                    <div class="text"><a href="<?php echo of_get_option('social_facebook'); ?>" target="_blank">Facebook</a></div>
                                </li>
                            <?php endif; ?>
                            <?php if (of_get_option('social_twitter')) : ?>
                                <li>
                                    <div class="icon"><a href="<?php echo of_get_option('social_twitter'); ?>" target="_blank"><em class="fa fa-twitter"></em></a></div>
                                    <div class="text"><a href="<?php echo of_get_option('social_twitter'); ?>" target="_blank">Twitter</a></div>
                                </li>
                            <?php endif; ?>
                            <?php if (of_get_option('social_linkedin')) : ?>
                                <li>
                                    <div class="icon"><a href="<?php echo of_get_option('social_linkedin'); ?>" target="_blank"><em class="fa fa-linkedin"></em></a></div>
                                    <div class="text"><a href="<?php echo of_get_option('social_linkedin'); ?>" target="_blank">Linkedin</a></div>
                                </li>
                            <?php endif; ?>
                            <?php if (of_get_option('social_instagram')) : ?>
                                <li>
                                    <div class="icon"><a href="<?php echo of_get_option('social_instagram'); ?>" target="_blank"><em class="fa fa-instagram"></em></a></div>
                                    <div class="text"><a href="<?php echo of_get_option('social_instagram'); ?>" target="_blank">Instagram</a></div>
                                </li>
                            <?php endif; ?>
                            <?php if (of_get_option('social_youtube')) : ?>
                                <li>
                                    <div class="icon"><a href="<?php echo of_get_option('social_youtube'); ?>" target="_blank"><em class="fa fa-youtube"></em></a></div>
                                    <div class="text"><a href="<?php echo of_get_option('social_youtube'); ?>" target="_blank">YouTube</a></div>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="column x1 logo">
                        <?php if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE == 'fr') : ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/logo-fr.png" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
                        <?php else : ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
                        <?php endif; ?>
                        <p><a href="https://winmar.ca">Visit our Corporate Site</a></p>
                        <?php if (of_get_option('business_number')) : ?>
                            <p><?php echo of_get_option('business_number'); ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </footer>
        <div id="footer-credit"><a href="http://www.shout-media.ca" target="_blank" rel="noopener">Website by shout-media.ca</a></div>
        <div class="franchise-notice">
            <div class="container">
                <p>This WINMAR<sup>&reg;</sup> franchise is independently owned and operated<?php echo of_get_option('franchise_owner') ? ' by ' . of_get_option('franchise_owner') : ''; ?>. WINMAR<sup>&reg;</sup> is a registered trademark owned by WINMAR<sup>&reg;</sup> (Canada) International, Ltd. and is used under license.</p>
            </div>
        </div>
    </div>

    <?php if (of_get_option('site_bugherd')) : ?>
        <script type='text/javascript'>
        (function (d, t) {
          var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
          bh.type = 'text/javascript';
          bh.src = "https://www.bugherd.com/sidebarv2.js?apikey=<?php echo of_get_option('site_bugherd'); ?>";
          s.parentNode.insertBefore(bh, s);
          })(document, 'script');
        </script>
    <?php endif; ?>

    <?php wp_footer(); ?>

</body>
</html>
