<?php

$background   = $block['background'];
$title        = $block['title'];
$testimonials = $block['testimonials'];

if($background)
	$bgsrc = wp_get_attachment_image_src($background, array(2000, 2000));

?>
<div class="testimonials-list">
	<div class="container">
		<?php if($title) : ?>
			<div class="title_block">
				<h2 class="title"><?php echo $title; ?></h2>
			</div>
		<?php endif; ?>
	<div class="list">

				<?php

				if($testimonials) :
					foreach($testimonials as $post) :
						setup_postdata($post);?>

						<div class="testimonial" id="testimonial-<?php echo $post->ID; ?>">
							<div class="testimonial-top">
								<div class="icon"><em class="fa fa-quote-left"></em></div>
								<p class="name"><?php the_title(); ?></p>
								<?php if(get_field('service')) : ?>
									<p class="service"><?php the_field('service'); ?></p>
								<?php endif; ?>
							</div>
							<div class="testimonial-content">
								<?php the_field('testimonial'); ?>
							</div>
							<hr>
						</div>

						<?php wp_reset_postdata($post);
					endforeach;
				endif;

				?>

		</div>
</div>
