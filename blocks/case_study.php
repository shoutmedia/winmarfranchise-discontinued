<?php

$post = $block['study'];

if($post) :

	setup_postdata($post);

	if(has_post_thumbnail()) :
		$thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), array(490, 490));
	endif;

	$summary = get_field('study_summary');

	?>
	<div class="case_study">
		<div class="shadows">
			<div class="cover">
				<div class="container">
					<div class="inner">
						<?php if(has_post_thumbnail()) : ?>
							<div class="photo" style="background-image: url(<?php echo $thumb[0]; ?>);"></div>
						<?php endif; ?>
						<div class="text">
							<div class="title_block">
								<h2 class="title"><span><?php _e('Case Study', DOMAIN); ?>:</span> <?php the_title(); ?></h2>
							</div>
							<div class="icons">
								<em class="fa fa-wrench"></em>
							</div>
							<?php if($summary) : ?>
								<div class="columns x<?php echo count($summary); ?>">
									<?php foreach($summary as $col) : ?>
										<div class="column">
											<?php echo $col['column']; ?>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
							<div class="buttons">
								<a href="<?php the_permalink(); ?>" class="btn"><strong><?php _e('View', DOMAIN); ?></strong> <?php _e('All', DOMAIN); ?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>