<?php

/**
 * Decide on text color based on background
 * @param  str Background Color
 * @return str Color
 */
function getContrast($hexcolor)
{
	if(substr($hexcolor, 0, 1) == '#')
		$hexcolor = substr($hexcolor, 1);

	$r = hexdec(substr($hexcolor, 0, 2));
	$g = hexdec(substr($hexcolor, 2, 2));
	$b = hexdec(substr($hexcolor, 4, 2));

	$yiq = (($r * 299) + ($g * 587) + ($b * 114)) / 1000;

	return ($yiq >= 128) ? '#4c4c4e' : 'white';
}

/**
 * Retrieve meta data about attachments
 * @param  int Attachment ID
 * @return arr Details
 */
function wp_get_attachment( $attachment_id )
{
	$attachment = get_post( $attachment_id );

	return array(
		'alt'         => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption'     => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href'        => get_permalink( $attachment->ID ),
		'src'         => $attachment->guid,
		'title'       => $attachment->post_title
	);
}

/**
 * Take a sentence and split directly in half
 * @param  str Sentence to split in half
 * @return arr Results
 */
function split_line($line = '')
{
	$words   = explode(" ", $line);
	$length  = ceil(strlen($line) / 2);
	$lengths = array();

	// Just return 1 word strings
	if(count($words) < 1)
		return array($line, '');

	foreach($words as $key => $word)
	{
		$prev = 0;

		if(array_key_exists($key - 1, $lengths))
			$prev = $lengths[$key - 1];

		$lnlen = $prev + strlen($word);

		// Add 1 to account for spaces
		if($key > 0)
			$lnlen++;

		$lengths[] = $lnlen;
	}

	$closest = null;

	foreach($lengths as $item)
	{
		if($closest == null || abs($length - $closest) > abs($item - $length))
			$closest = $item;
	}

	$line1 = substr($line, 0, $closest);
	$line2 = substr($line, $closest + 1);

	return array($line1, $line2);
}