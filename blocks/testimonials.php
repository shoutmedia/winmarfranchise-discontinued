<?php

$background   = $block['background'];
$title        = $block['title'];
$testimonials = $block['featured_testimonials'];
$accent_title = $block['accent_title'];
$accent_text  = $block['accent_text'];
$buttons      = $block['buttons'];
$readmore 	  = $block['read_more'];
$readmorelink = $block['read_more_link'];

if($background)
	$bgsrc = wp_get_attachment_image_src($background, array(2000, 2000));

?>
<div class="testimonials">
	<div class="container">
		<?php if($title) : ?>
			<div class="title_block">
				<h2 class="title"><?php echo $title; ?></h2>
			</div>
		<?php endif; ?>
	</div>
	<div class="slider"<?php echo $background ? ' style="background-image: url(' . $bgsrc[0] . ');"' : ''; ?>>
		<div class="container">
			<div class="wrapper">
				<div class="accent">
					<div class="text">
						<?php if($accent_title) : ?>
							<h3 class="title"><?php echo $accent_title; ?></h3>
						<?php endif; ?>
						<?php echo $accent_text; ?>
						<?php if($buttons) : ?>
							<div class="btns">
								<?php foreach($buttons as $btn) : ?>
									<a href="<?php echo $btn['destination']; ?>" class="btn <?php echo $btn['style']; ?>"><?php echo $btn['label']; ?></a>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="slide_con">
					<div class="slides">
						<?php

						if($testimonials) :
							foreach($testimonials as $post) :
								setup_postdata($post);

								include(get_template_directory() . '/inc/testimonial.php');

								wp_reset_postdata($post);
							endforeach;
						else :
							$query = new WP_Query(array(
								'post_type'      => 'testimonial',
								'posts_per_page' => 4,
								'order'          => 'ASC'
							));

							if($query && $query->post_count > 0) :
								while ( $query->have_posts() ) : $query->the_post();
									include(get_template_directory() . '/inc/testimonial.php');
								endwhile;
							endif;

							wp_reset_query();
						endif;

						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
