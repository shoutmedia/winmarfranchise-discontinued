<?php

if(!isset($blocks)) :
	$blocks = get_field('blocks');
endif;

if($blocks) :
	foreach($blocks as $block) :
		$path = get_template_directory() . '/blocks/' . $block['acf_fc_layout'] . '.php';

		if(file_exists($path))
			include($path);
		else
			echo '<p>"' . $block['acf_fc_layout'] . '" template missing.</p>';

	endforeach;
endif;