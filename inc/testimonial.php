<div class="testimonial">
	<div class="icon"><em class="fa fa-quote-left"></em></div>
	<?php if ($readmore) : ?>
		<?php echo wp_trim_words(get_field('testimonial'), 90, '...<p><a class="btn" href="' . $readmorelink . '#testimonial-' . $post->ID .'">Read More</a></p>'); ?>
	<?php else: ?>
		<?php the_field('testimonial'); ?>
	<?php endif; ?>
	<hr>
	<p class="name"><?php the_title(); ?></p>
	<?php if(get_field('service')) : ?>
		<p class="service"><?php the_field('service'); ?></p>
	<?php endif; ?>
</div>
