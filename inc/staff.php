<div class="staff-member">
	<div class="pull-right">
		<a href="#" class="contact-staff btn"><?php _e('Contact', DOMAIN); ?></a>
	</div>
	<div class="staff-name"><?php the_title(); ?></div> <div class="staff-title"><?php echo the_field('title'); ?></div>
	<div class="hidden staff-email"><?php echo the_field('email'); ?></div>
</div>