<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); 

		$blocks = [
            ['acf_fc_layout' => 'article_body']
        ];

        $title1 = 'News';
        $title2 = 'Updates';

		?>

		<?php include(get_template_directory() . '/inc/banner.php'); ?>
		<main class="main_content">
			<?php include(get_template_directory() . '/inc/blocks.php'); ?>
		</main>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>