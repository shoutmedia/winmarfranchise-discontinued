<div class="content_with_sidebar">
    <div class="container">
        <div class="content">
            <div class="title_block">
                <h2 class="title"><?php the_title(); ?></h2>
            </div>
            <?php the_content(); ?>
        </div>
        <aside class="sidebar">
            <div class="title_block">
                <h2 class="title"><?php _e('Related Posts', 'winmar'); ?></h2>
            </div>
            <?php

            $articles = new WP_Query([
                'posts_per_page' => 10
            ]);

            if ($articles->have_posts()) :

                echo '<ul>';

                while($articles->have_posts()) : $articles->the_post();

                    echo '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';

                endwhile;

                echo '</ul>';

            endif;

            ?>
        </aside>
    </div>
</div>