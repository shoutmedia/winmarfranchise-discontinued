<?php get_header(); ?>

<main class="main-content" role="main" id="main" tabindex="-1">

	<?php
	$title1 = 'News';
	$title2 = 'Updates';

	?>

	<?php include(get_template_directory() . '/inc/banner.php'); ?>

	<?php include(get_template_directory() . '/blocks/blog_index.php'); ?>

</main>

<?php get_footer(); ?>