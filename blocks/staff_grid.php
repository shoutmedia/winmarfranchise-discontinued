<?php

$background = $block['background'];
$staff 		= $block['staff'];

$title = $block['title'];

if($background)
	$bgsrc = wp_get_attachment_image_src($background, array(2000, 2000));

?>
<div class="staff-grid">
  <div class="container">
		<?php if($title): ?>
				<div class="title_block">
						<h3 class="title"><?php echo $title; ?></h3>
				</div>
		<?php endif; ?>
		<div class="grid-items">
			<?php foreach($staff as $post) :
				setup_postdata($post);
				$staff_photo = get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : get_template_directory_uri() . '/img/user-profile-img.png';
				?>
				<div class="staff-item">
					<div class="staff-photo" style="background-image:url(<?php echo $staff_photo ?>)"></div>
					<div class="staff-content">
						<div class="staff-name"><?php the_title(); ?></div>
						<div class="staff-title"><?php echo the_field('title'); ?></div>
						<div class="hidden staff-email"><?php echo the_field('email'); ?></div>
						<div class="staff-bio"><?php echo the_field('biography'); ?></div>
					</div>
				</div>
			<?php wp_reset_postdata($post);
			endforeach; ?>
		</div>
  </div>
</div>
