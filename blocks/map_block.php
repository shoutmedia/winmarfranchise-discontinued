<?php

$title    = $block['title'];
$desc     = $block['description'];
$btns     = $block['buttons'];
$location = $block['location'];
$center   = $block['map_center'];
$markers  = $block['locations'];

?>
<div class="map_block">
	<?php if($location || $markers) : ?>
		<div class="map">
			<?php if($center) : ?>
				<div class="map_center" data-lat="<?php echo $center['lat']; ?>" data-lng="<?php echo $center['lng']; ?>"></div>
			<?php endif; ?>
			<?php if($location) : ?>
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" data-label=""></div>
			<?php endif; ?>
			<?php if($markers) : ?>
				<?php foreach($markers as $mark) : ?>
					<div class="marker" data-lat="<?php echo $mark['marker']['lat']; ?>" data-lng="<?php echo $mark['marker']['lng']; ?>" data-label="<?php echo $mark['label']; ?>"></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="text">
			<?php if($title) : ?>
				<div class="title_block">
					<h2 class="title"><?php echo $title; ?></h2>
				</div>
			<?php endif; ?>
			<?php echo $desc; ?>
			<?php if($btns) : ?>
				<div class="btns">
					<?php foreach($btns as $btn) : ?>
						<a href="<?php echo $btn['destination']; ?>" class="btn <?php echo $btn['style']; ?>"><?php echo $btn['label']; ?></a>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>