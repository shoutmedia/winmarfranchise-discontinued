<?php get_header(); ?>

	<main class="main_content" style="padding: 80px 0; background: #E4E4E4; text-align: center;">
        <div class="container">
    		<h2><?php _e('Error 404', DOMAIN); ?><br><?php  _e('Page Not Found', DOMAIN); ?></h2>
        </div>
    </main>

<?php get_footer(); ?>