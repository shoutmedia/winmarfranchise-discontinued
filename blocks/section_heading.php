<?php

$title1 = $block['title_line_1'];
$title2 = $block['title_line_2'];
$title3 = $block['title_line_3'];
$desc   = $block['description'];
$btnlbl = $block['button_label'];
$btnurl = $block['button_destination'];

?>
<div class="section_heading">
	<div class="container">
		<div class="title_block">
			<h2 class="title">
				<?php if($title1) : ?>
					<span class="line1"><?php echo $title1; ?></span>
				<?php endif; ?>
				<?php if($title2) : ?>
					<span class="line2"><?php echo $title2; ?></span>
				<?php endif; ?>
				<?php if($title3) : ?>
					<span class="line3"><?php echo $title3; ?></span>
				<?php endif; ?>
			</h2>
		</div>
		<div class="info">
			<div class="desc">
				<?php echo $desc; ?>
			</div>
			<div class="buttons">
				<?php if($btnlbl && $btnurl) :

					$btnline = split_line($btnlbl);

					?>
					<a href="<?php echo $btnurl; ?>" class="btn"><strong><?php echo $btnline[0]; ?></strong> <?php echo $btnline[1]; ?></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>