<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name()
{

    // This gets the theme name from the stylesheet
    $themename = get_option('stylesheet');
    $themename = preg_replace("/\W/", "_", strtolower($themename));

    $optionsframework_settings = get_option('optionsframework');
    $optionsframework_settings['id'] = $themename;
    update_option('optionsframework', $optionsframework_settings);
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'html5reset'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options()
{

    // Pull all the categories into an array
    $options_categories = array();
    $options_categories_obj = get_categories();
    foreach ($options_categories_obj as $category) {
        $options_categories[$category->cat_ID] = $category->cat_name;
    }

    // Pull all tags into an array
    $options_tags = array();
    $options_tags_obj = get_tags();
    foreach ($options_tags_obj as $tag) {
        $options_tags[$tag->term_id] = $tag->name;
    }

    // Pull all the pages into an array
    $options_pages = array();
    $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
    $options_pages[''] = 'Select a page:';
    foreach ($options_pages_obj as $page) {
        $options_pages[$page->ID] = $page->post_title;
    }

    $options = array();

    $options[] = array(
        'name' => __('Contact Information', 'html5reset'),
        'type' => 'heading');

    $options[] = array(
        'name' => __('Email', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'contact_email',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Show email on footer', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'contact_email_footer',
        'std' => '',
        'type' => 'checkbox');
    $options[] = array(
        'name' => __('Phone', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'contact_phone',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Toll Free', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'contact_toll_free',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Fax', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'contact_fax',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Street Address', 'socialmedia'),
        'desc' => __("Appears in the footer", 'socialmedia'),
        'id' => 'contact_address_str',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Street Address (Line 2)', 'socialmedia'),
        'desc' => __("Appears in the footer", 'socialmedia'),
        'id' => 'contact_address_str2',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('City', 'socialmedia'),
        'desc' => __("Appears in the footer", 'socialmedia'),
        'id' => 'contact_address_city',
        'std' => 'Thunder Bay',
        'type' => 'text');
    $options[] = array(
        'name' => __('Province', 'socialmedia'),
        'desc' => __("Appears in the footer", 'socialmedia'),
        'id' => 'contact_address_province',
        'std' => 'ON',
        'type' => 'text');
    $options[] = array(
        'name' => __('Postal Code', 'socialmedia'),
        'desc' => __("Appears in the footer", 'socialmedia'),
        'id' => 'contact_address_postal',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Contact Us Link', 'socialmedia'),
        'desc' => __("Replaces the address in the footer", 'socialmedia'),
        'id' => 'contact_us_link',
        'std' => '',
        'type' => 'select',
        'options' => $options_pages);
    $options[] = array(
        'name' => __('Business Number', 'socialmedia'),
        'desc' => __("Appears in the footer", 'socialmedia'),
        'id' => 'business_number',
        'type' => 'text');
    $options[] = array(
        'name' => __('Franchise Owner', 'socialmedia'),
        'desc' => __("Appears in the footer", 'socialmedia'),
        'id' => 'franchise_owner',
        'type' => 'text');

    $options[] = array(
        'name' => __('Social Media', 'html5reset'),
        'type' => 'heading');

    // Social Media
    $options[] = array(
        'name' => __('Facebook', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'social_facebook',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Twitter', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'social_twitter',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('LinkedIn', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'social_linkedin',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Instagram', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'social_instagram',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('YouTube', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'social_youtube',
        'std' => '',
        'type' => 'text');

    $options[] = array(
        'name' => __('Header Meta', 'html5reset'),
        'type' => 'heading');

    // Standard Meta
    $options[] = array(
        'name' => __('Head ID', 'html5reset'),
        'desc' => __("", 'html5reset'),
        'id' => 'meta_headid',
        'std' => 'www-sitename-com',
        'type' => 'text');
    $options[] = array(
        'name' => __('Author Name', 'html5reset'),
        'desc' => __('Populates meta author tag.', 'html5reset'),
        'id' => 'meta_author',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Mobile Viewport', 'html5reset'),
        'desc' => __('Uncomment to use; use thoughtfully!', 'html5reset'),
        'id' => 'meta_viewport',
        'std' => 'width=device-width, initial-scale=1.0',
        'type' => 'text');

    // Icons
    $options[] = array(
        'name' => __('Site Favicon', 'html5reset'),
        'desc' => __('', 'html5reset'),
        'id' => 'head_favicon',
        'type' => 'upload');
    $options[] = array(
        'name' => __('Apple Touch Icon', 'html5reset'),
        'desc' => __('', 'html5reset'),
        'id' => 'head_apple_touch_icon',
        'type' => 'upload');

    // App: Windows 8
    $options[] = array(
        'name' => __('App: Windows 8', 'html5reset'),
        'desc' => __('Application Name', 'html5reset'),
        'id' => 'meta_app_win_name',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('', 'html5reset'),
        'desc' => __('Tile Color', 'html5reset'),
        'id' => 'meta_app_win_color',
        'std' => '',
        'type' => 'color');
    $options[] = array(
        'name' => __('', 'html5reset'),
        'desc' => __('Tile Image', 'html5reset'),
        'id' => 'meta_app_win_image',
        'std' => '',
        'type' => 'upload');

    // Bugherd
    $options[] = array(
        'name' => __('Bugherd ID', 'html5reset'),
        'desc' => '',
        'id' => 'site_bugherd',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Analytics - Tracking ID', 'html5reset'),
        'desc' => '',
        'id' => 'analytics_id',
        'std' => '',
        'type' => 'text');

    $options[] = array(
        'name' => __('&nbsp;Site Ribbon', 'html5reset'),
        'type' => 'heading');

    // Header Ribbon Content
    $options[] = array(
        'name' => __('Ribbon Content', 'html5reset'),
        'desc' => __("Leave blank and the ribbon will be hidden.", 'html5reset'),
        'id' => 'ribbon_content',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Pop up key', 'html5reset'),
        'desc' => __("Used to remember if a user closed the pop up. Changing this value will reset their choice and show the pop up.", 'html5reset'),
        'id' => 'pop_up_key',
        'std' => 'cornerpop2021',
        'type' => 'text');
    $options[] = array(
        'name' => __('Pop up content', 'html5reset'),
        'desc' => __("Leave blank and the pop up will be hidden.", 'html5reset'),
        'id' => 'pop_up_content',
        'std' => '',
        'type' => 'editor');
    $options[] = array(
        'name' => __('Pop up button text', 'html5reset'),
        'desc' => __("Leave blank and the button will be hidden.", 'html5reset'),
        'id' => 'pop_up_button_label',
        'std' => '',
        'type' => 'text');
    $options[] = array(
        'name' => __('Pop up button link', 'html5reset'),
        'desc' => __("Leave blank and the button will be hidden.", 'html5reset'),
        'id' => 'pop_up_button_link',
        'std' => '',
        'type' => 'text');

    return $options;
}
