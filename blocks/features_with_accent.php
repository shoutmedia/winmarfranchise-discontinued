<?php

$accent = $block['accent_image'];
$url = $block['accent_url'];
$features = $block['features'];

if ($accent) :
    $accsrc = wp_get_attachment_image_src($accent, array(450, 800));
    $accmet = wp_get_attachment($accent);
endif;

?>
<div class="features_with_accent <?php echo isset($block['alignment']) ? $block['alignment'] : ''; ?>">
    <div class="container">
        <?php if ($accent) : ?>
            <div class="accent">
                <?php if ($url) : ?>
                    <a href="<?php echo $url; ?>"><img src="<?php echo $accsrc[0]; ?>" alt="<?php echo $accmet['alt']; ?>"></a>
                <?php else : ?>
                    <img src="<?php echo $accsrc[0]; ?>" alt="<?php echo $accmet['alt']; ?>">
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <?php if ($features) : ?>
            <div class="features">
                <?php foreach ($features as $feature) :
                    $photo = $feature['photo'];
                    $label = $feature['label'];
                    $link  = $feature['link_destination'];

                    if ($label) {
                        $lines = split_line($label);
                    }

                    if ($photo) {
                        $phsrc = wp_get_attachment_image_src($photo, array(265, 265));
                    }

                    ?>
                    <div class="feature">
                        <a href="<?php echo $link; ?>">
                            <?php if ($photo) : ?>
                                <span class="img" style="background-image: url(<?php echo $phsrc[0]; ?>);"></span>
                            <?php endif; ?>
                            <?php if ($label) : ?>
                                <h3 class="label"><strong><?php echo $lines[0]; ?></strong> <?php echo $lines[1]; ?></h3>
                            <?php endif; ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
