<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head id="<?php echo of_get_option('meta_headid'); ?>">
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php

    if (is_search()) {
        echo '<meta name="robots" content="noindex, nofollow" />';
    }

    ?>
	<title><?php wp_title('|', true, 'right'); ?></title>
	<meta name="title" content="<?php wp_title('|', true, 'right'); ?>">
	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
	<?php

    if (of_get_option('meta_author')) {
        echo '<meta name="author" content="' . of_get_option("meta_author") . '" />';
    }

    if (of_get_option('meta_viewport')) {
        echo '<meta name="viewport" content="' . of_get_option("meta_viewport") . '" />';
    }

    if (of_get_option('head_favicon')) :
        echo '<meta name="mobile-web-app-capable" content="yes">';
        echo '<link rel="shortcut icon" sizes="1024x1024" href="' . of_get_option("head_favicon") . '" />';
    endif;

    if (of_get_option('head_apple_touch_icon')) {
        echo '<link rel="apple-touch-icon" href="' . of_get_option("head_apple_touch_icon") . '">';
    }

    // Windows 8
    if (of_get_option('meta_app_win_name')) :
        echo '<meta name="application-name" content="' . of_get_option("meta_app_win_name") . '" /> ';
        echo '<meta name="msapplication-TileColor" content="' . of_get_option("meta_app_win_color") . '" /> ';
        echo '<meta name="msapplication-TileImage" content="' . of_get_option("meta_app_win_image") . '" />';
    endif;

    ?>
	<script src="<?php echo get_template_directory_uri(); ?>/_/js/modernizr.js"></script>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">

	<?php wp_head(); ?>

	<?php if (of_get_option('analytics_id')) : ?>
		<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo of_get_option('analytics_id'); ?>"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', '<?php echo of_get_option('analytics_id'); ?>');
		</script>
	<?php endif; ?>

</head>

<body <?php body_class(); ?>>

	<div class="sidenav">
		<div class="heading"><?php _e('Menu', DOMAIN); ?></div>
		<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
	</div>

    <?php if (of_get_option('pop_up_content') && of_get_option('pop_up_key')) : ?>
        <div class="corner-pop" data-key="<?php echo of_get_option('pop_up_key'); ?>">
            <div class="box">
                <em class="fa fa-times"></em>
                <?php echo apply_filters('the_content', of_get_option('pop_up_content')); ?>
                <?php if (of_get_option('pop_up_button_label') && of_get_option('pop_up_button_link')) : ?>
                    <p><a href="<?php echo of_get_option('pop_up_button_link'); ?>" class="btn"><?php echo of_get_option('pop_up_button_label'); ?></a></p>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

	<div class="main_wrapper">

    <?php if (of_get_option('ribbon_content')) : ?>
      <div class="site-ribbon">
        <div class="container">
          <?php echo of_get_option('ribbon_content'); ?>
        </div>
      </div>
    <?php endif; ?>
		<div class="topbar">
			<div class="container">
				<div class="mobile-nav"><em class="fa fa-bars"></em></div>
				<div class="inner">
					<nav class="nav">
						<img src="<?php echo get_template_directory_uri(); ?>/img/24.png" alt="">
						<span class="emerg"><?php _e('Emergency Services #', DOMAIN); ?></span>
						<?php wp_nav_menu(array('theme_location' => 'topbar')); ?>
					</nav>
					<?php if (of_get_option('social_facebook') || of_get_option('social_twitter') || of_get_option('social_linkedin')) : ?>
						<ul class="social">
							<?php if (of_get_option('social_facebook')) : ?>
								<li><a href="<?php echo of_get_option('social_facebook'); ?>" target="_blank"><em class="fa fa-facebook"></em></a></li>
							<?php endif; ?>
							<?php if (of_get_option('social_twitter')) : ?>
								<li><a href="<?php echo of_get_option('social_twitter'); ?>" target="_blank"><em class="fa fa-twitter"></em></a></li>
							<?php endif; ?>
							<?php if (of_get_option('social_linkedin')) : ?>
								<li><a href="<?php echo of_get_option('social_linkedin'); ?>" target="_blank"><em class="fa fa-linkedin"></em></a></li>
							<?php endif; ?>
							<?php if (of_get_option('social_instagram')) : ?>
								<li><a href="<?php echo of_get_option('social_instagram'); ?>" target="_blank"><em class="fa fa-instagram"></em></a></li>
							<?php endif; ?>
							<?php if (of_get_option('social_youtube')) : ?>
								<li><a href="<?php echo of_get_option('social_youtube'); ?>" target="_blank"><em class="fa fa-youtube"></em></a></li>
							<?php endif; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<header class="header" role="header">
			<div class="container">
				<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home" class="logo">
					<?php if (defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE == 'fr') : ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-fr.png" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
                    <?php else : ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
                    <?php endif; ?>
				</a>
				<nav class="navigation" role="navigation">
					<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
				</nav>
			</div>
		</header>
