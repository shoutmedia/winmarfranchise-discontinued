<?php

$title    = $block['title'];
$desc     = $block['description'];
$services = $block['services'];

$background   = $block['background_image'];
$bgcolor      = $block['background_color'];
$bgposition   = $block['background_position'];

if($background) :
    if($bgposition == 'fill')
        $bgsrc = wp_get_attachment_image_src($background, array(2000, 2000));
    else
        $bgsrc = wp_get_attachment_image_src($background, 'full');
endif;

?>
<div class="service_breakdown"
	<?php

    if($background || $bgcolor) :
        echo ' style="';
        echo $background ? 'background-image: url(' . $bgsrc[0] . ');' : '';
        echo $bgcolor    ? 'background-color: ' . $bgcolor . ';' : '';
        echo '"';
    endif;

    ?>
	>
	<div class="container">
		<?php if($title) : ?>
			<div class="title_block">
				<h2 class="title"><?php echo $title; ?></h2>
			</div>
		<?php endif; ?>
		<?php if($desc) : ?>
			<div class="desc">
				<?php echo $desc; ?>
			</div>
		<?php endif; ?>
		<?php if($services) : ?>
			<div class="services x<?php echo count($services); ?>">
				<?php foreach($services as $service) :

					$image = $service['image'];
					$title = $service['title'];
					$desc  = $service['description'];
					$link  = $service['link'];
					$size  = $service['dont_restrict_image_size'];

					if($image)
						$imgsrc = wp_get_attachment_image_src($image, array(240, 240));

					?>
					<div class="service">
						<?php if($link) : ?>
							<a href="<?php echo $link; ?>" class="img<?php echo $size ? ' nocover' : ''; ?>"<?php echo $image ? ' style="background-image: url(' . $imgsrc[0] . ');"' : ''; ?>></a>
						<?php elseif($image) : ?>
							<div class="img<?php echo $size ? ' nocover' : ''; ?>"<?php echo $image ? ' style="background-image: url(' . $imgsrc[0] . ');"' : ''; ?>></div>
						<?php endif; ?>
						<?php if($title) : ?>
							<?php if($link) : ?>
								<h4><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h4>
							<?php else : ?>
								<h4><?php echo $title; ?></h4>
							<?php endif; ?>
						<?php endif; ?>
						<?php echo $desc; ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>
