var fs = require('fs');

function patchOtf2ttf() {
    fs.readFile('node_modules/otf2ttf/otf2ttf.sh', 'utf-8', function(err, data) {
        if (err) throw err;

        // Replace 2 offending lines
        data = data.replace('\nPrint(\'    "fontSourceFile":    "\' + $1 + \'",\');', '\n#Print(\'    "fontSourceFile":    "\' + $1 + \'",\');');
        data = data.replace('\nPrint(\'    "fontPath": "\' + $2 + \'",\');', '\n#Print(\'    "fontPath": "\' + $2 + \'",\');');

        // Remove comma from Line 10
        data = data.replace('\nPrint(\'    "fontFile": "\' + fontFile + \'",\');', '\nPrint(\'    "fontFile": "\' + fontFile + \'"\');');

        // Comment out Copyright
        data = data.replace('\nPrint(\'    "copyright":   "\' + $copyright + \'"\');', '\n#Print(\'    "copyright":   "\' + $copyright + \'"\');');

        fs.writeFile('node_modules/otf2ttf/otf2ttf.sh', data, 'utf-8', function (err) {
            if (err) throw err;
            console.log('otf2ttf patched');
        });
    });
}

patchOtf2ttf();
