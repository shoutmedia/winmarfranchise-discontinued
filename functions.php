<?php

define('DOMAIN', 'winmar');

// Helper functions
require_once dirname( __FILE__ ) . '/_/inc/helpers.php';

// Advanced Custom Field Search Extension
require_once dirname( __FILE__ ) . '/_/inc/acf_search.php';

// Theme Updater
require_once(dirname( __FILE__ ) . '/inc/wp-updates-theme.php');

// Check Updates
new WPUpdatesThemeUpdater_1887( 'http://wp-updates.com/api/2/theme', basename( get_template_directory() ) );

// Options Framework (https://github.com/devinsays/options-framework-plugin)
if ( !function_exists( 'optionsframework_init' ) )
{
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/_/inc/' );
	require_once dirname( __FILE__ ) . '/_/inc/options-framework.php';
}

// Theme Setup
function winmar_setup()
{
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'structured-post-formats', array( 'link', 'video' ) );
	add_theme_support( 'post-thumbnails' );

	// Menus
	register_nav_menu( 'primary', __( 'Navigation Menu', DOMAIN ) );
	register_nav_menu( 'topbar', __( 'Upper Most Menu', DOMAIN ) );
	register_nav_menu( 'footer1', __( 'Footer Column 1', DOMAIN ) );
	register_nav_menu( 'footer2', __( 'Footer Column 2', DOMAIN ) );

	// When inserting an image don't link it
	update_option('image_default_link_type', 'none');

	// Remove Gallery Styling
	add_filter( 'use_default_gallery_style', '__return_false' );
}

add_action( 'after_setup_theme', 'winmar_setup' );

// Load jQuery
if ( !function_exists( 'core_mods' ) )
{
	function core_mods()
	{
		if ( !is_admin() )
		{
			wp_deregister_script( 'jquery' );
			wp_register_script( 'jquery', ( "//code.jquery.com/jquery-2.1.4.js" ), false);
			wp_enqueue_script( 'jquery' );
		}
	}

	add_action( 'wp_enqueue_scripts', 'core_mods' );
}

// Load theme specific assets
function winmar_scripts_styles()
{
	global $wp_styles;

	// Load Stylesheets
	wp_enqueue_style( 'winmar-slick', get_template_directory_uri() . '/slick/slick.css' );
	wp_enqueue_style( 'winmar-slick-theme', get_template_directory_uri() . '/slick/slick-theme.css' );
	wp_enqueue_style( 'winmar-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'winmar-style', get_stylesheet_uri() );

	// Load JavaScript
	wp_enqueue_script( 'winmar-gmap', 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAyC5GdvNgjH9A1LvMMPnBzBvAOI7kJNIQ' );
	wp_enqueue_script( 'winmar-labels', get_template_directory_uri() . '/js/markerwithlabel.js' );
	wp_enqueue_script( 'winmar-slick', get_template_directory_uri() . '/slick/slick.min.js' );
	wp_enqueue_script( 'winmar-main', get_template_directory_uri() . '/js/main.js' );
}

add_action( 'wp_enqueue_scripts', 'winmar_scripts_styles' );

// Clean up <head>
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}

add_action('init', 'removeHeadLinks');

// Widgets
if ( function_exists('register_sidebar' ))
{
	function winmar_widgets_init()
	{
		register_sidebar( array(
			'name'          => __( 'Sidebar Widgets', 'winmar' ),
			'id'            => 'sidebar-primary',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>'
		) );
	}

	add_action( 'widgets_init', 'winmar_widgets_init' );
}

// Remove Emoji Support
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Custom Post Types
 */
function create_post_type()
{
	register_post_type( 'case-study',
		array(
			'labels' => array(
				'name'               => __( 'Case Studies' ),
				'singular_name'      => __( 'Case Study' ),
				'add_new_item'       => __( 'Add New Case Study' ),
				'edit_item'          => __( 'Edit Case Study' ),
				'new_item'           => __( 'New Case Study' ),
				'view_item'          => __( 'View Case Study' ),
				'search_items'       => __( 'Search Case Studies' ),
				'not_found'          => __( 'No Case Studies found' ),
				'not_found_in_trash' => __( 'No Case Studies found in trash' )
			),
			'public'   => true,
			'supports' => array(
				'title',
				'thumbnail'
			),
			'exclude_from_search' => true,
			'capability_type'     => 'post'
		)
	);

	register_post_type( 'service',
		array(
			'labels' => array(
				'name'               => __( 'Services' ),
				'singular_name'      => __( 'Service' ),
				'add_new_item'       => __( 'Add New Service' ),
				'edit_item'          => __( 'Edit Service' ),
				'new_item'           => __( 'New Service' ),
				'view_item'          => __( 'View Service' ),
				'search_items'       => __( 'Search Services' ),
				'not_found'          => __( 'No Services found' ),
				'not_found_in_trash' => __( 'No Services found in trash' )
			),
			'public'   => true,
			'supports' => array(
				'title',
				'thumbnail'
			),
			'exclude_from_search' => true,
			'capability_type'     => 'post'
		)
	);

	register_post_type( 'testimonial',
		array(
			'labels' => array(
				'name'               => __( 'Testimonials' ),
				'singular_name'      => __( 'Testimonial' ),
				'add_new_item'       => __( 'Add New Testimonial' ),
				'edit_item'          => __( 'Edit Testimonial' ),
				'new_item'           => __( 'New Testimonial' ),
				'view_item'          => __( 'View Testimonial' ),
				'search_items'       => __( 'Search Testimonials' ),
				'not_found'          => __( 'No Testimonials found' ),
				'not_found_in_trash' => __( 'No Testimonials found in trash' )
			),
			'public'   => true,
			'supports' => array(
				'title',
				'thumbnail'
			),
			'exclude_from_search' => true,
			'capability_type'     => 'post'
		)
	);

	register_post_type( 'staff',
		array(
			'labels' => array(
				'name'               => __( 'Staff' ),
				'singular_name'      => __( 'Staff' ),
				'add_new_item'       => __( 'Add New Staff Member' ),
				'edit_item'          => __( 'Edit Staff Member' ),
				'new_item'           => __( 'New Staff Member' ),
				'view_item'          => __( 'View Staff Member' ),
				'search_items'       => __( 'Search Staff Member' ),
				'not_found'          => __( 'No Staff Members found' ),
				'not_found_in_trash' => __( 'No Staff Members found in trash' )
			),
			'public'   => true,
			'supports' => array(
				'title',
				'thumbnail'
			),
			'exclude_from_search' => true,
			'capability_type'     => 'post'
		)
	);
}

add_action( 'init', 'create_post_type' );

function my_acf_init()
{
	acf_update_setting('google_api_key', 'AIzaSyAyC5GdvNgjH9A1LvMMPnBzBvAOI7kJNIQ');
}

add_action('acf/init', 'my_acf_init');

function load_admin_styles()
{
	wp_enqueue_style( 'admin_css_custom', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
	wp_enqueue_script( 'admin_js_custom', get_template_directory_uri() . '/js/admin-scripts.js', false, '1.0.0' );
}

add_action( 'admin_enqueue_scripts', 'load_admin_styles' );