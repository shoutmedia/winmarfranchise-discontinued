<?php

$background  = $block['background'];
$title       = $block['title'];
$description = $block['description'];
$buttons     = $block['buttons'];

$bg_type 	  = $block['background_type']; 
$bg_video_webm = $block['background_video_webm'];
$bg_video_mp4 = $block['background_video_mp4'];

if($background)
	$bgsrc = wp_get_attachment_image_src($background, array(2000, 2000));

if($title)
	$lines = split_line($title);

?>
<div class="banner"<?php echo $background ? ' style="background-image: url(' . $bgsrc[0] . ');"' : ''; ?>>
	<?php if($bg_type == 'video'): ?>
	<div class="video_container">
		<video playsinline autoplay muted loop poster="" class="bgvid">
		    <source src="<?php echo $bg_video_webm; ?>" type="video/webm">
		    <source src="<?php echo $bg_video_mp4; ?>" type="video/mp4">
		</video>
	</div>
	<?php endif; ?>
	<div class="container">
		<div class="text">
			<?php if($title) : ?>
				<h1><?php echo $lines[0]; ?> <strong><?php echo $lines[1]; ?></strong></h1>
			<?php endif; ?>
			<?php echo $description; ?>
			<?php if($buttons) : ?>
				<div class="buttons">
					<?php foreach($buttons as $btn) : ?>
						<a href="<?php echo $btn['destination']; ?>" class="btn blue"><?php echo $btn['label']; ?></a>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>