<?php

$background = $block['background'];
$staff 		= $block['staff_list'];
$content    = $block['editor'];

$title = $block['title'];

if($background)
	$bgsrc = wp_get_attachment_image_src($background, array(2000, 2000));


$query = new WP_Query(array(
	'post_type'      => 'staff',
	'order'          => 'ASC'
));

if(!$staff && !$query->have_posts()) {
	$staff_class = ' no-staff';
}

?>
<div class="staff-contact">
	<div class="container">
		<div class="staff<?php echo $staff_class; ?>">
			<?php if($title): ?>
	        <div class="title_block">
	            <h3 class="title"><?php echo $title; ?></h3>
	        </div>
	        <?php endif; ?>
			<div class="staff-list">
				<?php

				if($staff) :
					foreach($staff as $post) :
						setup_postdata($post);

						include(get_template_directory() . '/inc/staff.php');

						wp_reset_postdata($post);
					endforeach;
				else :


					if($query && $query->post_count > 0) :
						while ( $query->have_posts() ) : $query->the_post();
							include(get_template_directory() . '/inc/staff.php');
						endwhile;
					endif;

					wp_reset_query();
				endif;

				?>
			</div>
		</div>
		<div class="content<?php echo $staff_class; ?>">
			<?php echo $content; ?>
		</div>
	</div>
</div>