<?php
if(!isset($bg))
	$bg     = get_field('banner_background');

if(!isset($title1))
	$title1 = get_field('title_line_1');

if(!isset($title2))
	$title2 = get_field('title_line_2');

$bg_type 	   = get_field('background_type');
$bg_video_webm = get_field('background_video_webm');
$bg_video_mp4  = get_field('background_video_mp4');

if($bg)
	$bgsrc = wp_get_attachment_image_src($bg, array(2000, 2000));

?>
<div class="inner_banner"<?php echo $bg ? ' style="background-image: url(' . $bgsrc[0] . ');"' : ''; ?>>
	<?php if($bg_type == 'video'): ?>
	<div class="video_container">
		<video playsinline autoplay muted loop poster="" class="bgvid">
		    <source src="<?php echo $bg_video_webm; ?>" type="video/webm">
		    <source src="<?php echo $bg_video_mp4; ?>" type="video/mp4">
		</video>
	</div>
	<?php endif; ?>
	<div class="container">
		<div class="title_block">
			<?php if($title1 || $title2) : ?>
				<h1 class="title">
				<?php if($title1) : ?>
					<span class="line1">
						<span><?php echo $title1; ?></span>
					</span>
				<?php endif; ?>
				<?php if($title2) : ?>
					<span class="line2"><?php echo $title2; ?></span>
				<?php endif; ?>
				</h1>
			<?php endif; ?>
		</div>
	</div>
</div>

<div class="breadcrumbs">
	<div class="container">
		<div class="trail">
			<?php bcn_display(); ?>
		</div>
	</div>
</div>