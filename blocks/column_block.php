<?php

$background   = $block['background_image'];
$bgcolor      = $block['background_colour'];
$bgposition   = $block['background_position'];
$textcolor    = $block['class_text_colour'];

$columns    = $block['columns'];
$titlestyle = $block['title_style'];
$title      = $block['title'];

if($background) :
    if($bgposition == 'fill')
        $bgsrc = wp_get_attachment_image_src($background, array(2000, 2000));
    else
        $bgsrc = wp_get_attachment_image_src($background, 'full');
endif;

?>
<div class="column-block<?php echo $textcolor ? ' ' . $textcolor : ''; ?>"
    <?php

    if($background || $bgcolor) :
        echo ' style="';
        echo $background ? 'background-image: url(' . $bgsrc[0] . ');' : '';
        echo $bgcolor    ? 'background-color: ' . $bgcolor . ';' : '';
        echo '"';
    endif;

    ?>
    >
    <div class="container">
        <?php if($title): ?>
        <div class="title_block<?php echo $titlestyle ? ' ' . $titlestyle : ''; ?>">
            <h2 class="title"><?php echo $title; ?></h2>
        </div>
        <?php endif; ?>
        <div class="row">
            <?php foreach($columns as $col) : ?>
                <div class="col w<?php echo $col['size'] ? $col['size'] : 12 / count($columns); ?>">
                    <?php if($col['title']) : ?>
                        <h3 class="title"><?php echo $col['title']; ?></h3>
                    <?php endif; ?>
                    <?php echo $col['content']; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
